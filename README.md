# BuuSocket
BuuSocket is an API for Spigot and BungeeCord that allows to communicate without going through PluginMessages, so the data no longer need a player and this makes this system secure.
## Tutorial
The first thing to do is to import the API into your project and into the plugins of your server.

Then you must create the BuuClient (On the Spigot side)
```java
BuuClient client = new BuuClient(port); //Default port: 2605
```

And the BuuServer (On the BungeeCord side)
```java
BuuServer server = new BuuServer(port); //Default port: 2605
```

### Send
Client side (Spigot):
```java
public void sendData() {
	BuuClient client = new BuuClient(port); //Default port: 2605
	DataOutputStream out = client.getOutput();

	out.writeUTF("Like PluginMessages");
	out.write...
	out.write...

	client.send(out);
}
```
Server side (Bungee)
```java
public void sendData() {
	BuuServer server = new BuuServer(port); //Default port: 2605

	DataOutputStream out = server.getOutput("ClientName - Sent from the client side");
	//OR
	Collection<DataOutputStream> outs = server.getOutputs(); //All clients (Spigot servers)

	out.writeUTF("Like PluginMessages");
	out.write...
	out.write...

	server.send(out);
}
```
### Receive
To receive data BuuSocket, add two events

On Spigot (BuuClient)
```java
@EventHandler
public void onBuuClientReceivingData(BuuClientReceivingDataEvent event) {
	DataInputStream in = event.getData();

	in.readUTF("Like PluginMessages");
	in.read...
	in.read...
}
```

On BungeeCord (BuuServer)
```java
@EventHandler
public void onBuuServerReceivingData(BuuServerReceivingDataEvent event) {
	DataInputStream in = event.getData();

	in.readUTF("Like PluginMessages");
	in.read...
	in.read...
}
```
