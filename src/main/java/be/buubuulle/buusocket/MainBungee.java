package be.buubuulle.buusocket;

import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;

public class MainBungee extends Plugin implements Listener {
    private static MainBungee instance;

    public static MainBungee getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
    }
}
