package be.buubuulle.buusocket.socket;

import be.buubuulle.buusocket.MainBungee;
import be.buubuulle.buusocket.events.BuuServerReceivingDataEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.md_5.bungee.api.plugin.Event;

public class BuuServer {
    private String host = "127.0.0.1";

    private int port = 2605;

    private ServerSocket serverSocket;

    private DataInputStream in;

    private Map<String, DataOutputStream> outs;

    public BuuServer() {
        startServer();
    }

    public BuuServer(int port) {
        this.port = port;
        startServer();
    }

    public BuuServer(String host, int port) {
        this.host = host;
        this.port = port;
        startServer();
    }

    private void startServer() {
        this.outs = new HashMap<>();
        try {
            this.serverSocket = new ServerSocket(this.port, 50, InetAddress.getByName(this.host));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread waitClient = new Thread(() -> {
            try {
                while (true) {
                    Socket clientSocket = this.serverSocket.accept();
                    starCommunication(clientSocket);
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        });
        waitClient.start();
    }

    private void starCommunication(Socket clientSocket) {
        try {
            DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());
            in = new DataInputStream(clientSocket.getInputStream());
            while (!outs.containsValue(out)) {
                if (in.available() != 0)
                    outs.put(this.in.readUTF(), out);
            }
            Thread inThread = new Thread(() -> {
                while (true) {
                    try {
                        while (true) {
                            if (in.available() != 0) {
                                MainBungee.getInstance().getProxy().getPluginManager().callEvent(new BuuServerReceivingDataEvent(in));
                                while (in.available() != 0)
                                    in.read();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            inThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(DataOutputStream out) throws IOException {
        out.flush();
    }

    public DataOutputStream getOutput(String clientName) {
        return outs.get(clientName);
    }

    public Collection<DataOutputStream> getOutputs() {
        return outs.values();
    }

    public Set<String> getClients() {
        return outs.keySet();
    }
}
