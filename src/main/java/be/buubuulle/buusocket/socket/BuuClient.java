package be.buubuulle.buusocket.socket;

import be.buubuulle.buusocket.events.BuuClientReceivingDataEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import org.bukkit.Bukkit;

public class BuuClient {
    private String host = "127.0.0.1";

    private int port = 2605;

    private String clientName = "";

    private DataInputStream in;

    private DataOutputStream out;

    public BuuClient() {
        startClient();
    }

    public BuuClient(int port) {
        this.port = port;
        startClient();
    }

    public BuuClient(String host, int port) {
        this.host = host;
        this.port = port;
        startClient();
    }

    private void startClient() {
        this.clientName = "Spigot-" + Bukkit.getPort();
        try {
            Socket clientSocket = new Socket(this.host, this.port);
            out = new DataOutputStream(clientSocket.getOutputStream());
            in = new DataInputStream(clientSocket.getInputStream());
            out.writeUTF(this.clientName);
            send(out);
            Thread inThread = new Thread(() -> {
                while (true) {
                    try {
                        while (true) {
                            if (in.available() != 0) {
                                Bukkit.getPluginManager().callEvent(new BuuClientReceivingDataEvent(in));
                                while (this.in.available() != 0)
                                    in.read();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            inThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(DataOutputStream out) throws IOException {
        out.flush();
    }

    public DataOutputStream getOutput() {
        return out;
    }

    public String getClientName() {
        return clientName;
    }
}
