package be.buubuulle.buusocket.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.io.DataInputStream;

public class BuuClientReceivingDataEvent extends Event {
    private DataInputStream data;

    public BuuClientReceivingDataEvent(DataInputStream data) {
        this.data = data;
    }

    public DataInputStream getData() {
        return data;
    }

    private static final HandlerList HANDLERS = new HandlerList();

    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
