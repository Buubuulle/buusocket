package be.buubuulle.buusocket.events;

import net.md_5.bungee.api.plugin.Event;

import java.io.DataInputStream;

public class BuuServerReceivingDataEvent extends Event {
    private DataInputStream data;

    public BuuServerReceivingDataEvent(DataInputStream data) {
        this.data = data;
    }

    public DataInputStream getData() {
        return data;
    }
}
